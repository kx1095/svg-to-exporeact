'use strict';
import chalk from 'chalk';
/*import React from 'react';
 import Svg   from 'expo';*/ // FUTURE dynamic load of supported elements when node support complete ES6 - can't load because "import"-statement are unexpected.

/* hard coded supported elements from react-native-svg */
const elementNames = [
	"Circle",
	"Ellipse",
	"G",
	"Text",
	"TSpan",
	"TextPath",
	"Path",
	"Polygon",
	"Polyline",
	"Line",
	"Rect",
	"Use",
	"Image",
	"Symbol",
	"Defs",
	"LinearGradient",
	"RadialGradient",
	"Stop",
	"ClipPath",
	"Pattern",
	"Mask",
	"Svg",
];
Object.defineProperty(Array.prototype, 'flat', {
	value: function(depth = 1) {
		return this.reduce(function(flat, toFlatten) {
			return flat.concat((Array.isArray(toFlatten) && (depth - 1)) ? toFlatten.flat(depth - 1) : toFlatten);
		}, []);
	},
});

const validateElementsToImport = (obj, debug) => {
	let result = [];
	for( let prop in obj ) {
		const value = obj[prop];
		if (prop !== "$") {
			if (prop[0] === prop[0].toUpperCase() && elementNames.indexOf(prop) > -1) {
				result.push(prop);
			}
			else {
				if (debug) {
					console.log(chalk.red(prop, "don't compatible with 'Svg'-library of Expo"));
				}
			}

			if (typeof value === 'object' || typeof value === 'symbol') {
				//result.push(toArrayKeys(value));
				try {
					let inner = validateElementsToImport(value);
					if (inner.length > 0) {
						result.push(inner);
					}
				}
				catch(e) {
					console.error(e);
				}
			}
		}
	}
	return result;
};

const getAttributeFromChildObj = (childArray, dgb = false) => {
	if (childArray instanceof Array) {
		let res = [];
		let unknownPos = [];
		if (childArray.length > 1) {
			childArray.map((objIn, ind) => {
				Object.keys(objIn).map(innerKey => {
					innerKey === "$" ?
					(res.push(objIn[innerKey]))
					                 :
					(res.length > 0 ?
					 (
						 (
							 res[res.length - 1].children === undefined ? res[res.length - 1] = Object.assign({}, res[res.length - 1], { children: [] }) : null
						 ),
							 (
								 res[res.length - 1].children.push({ [innerKey]: objIn[innerKey] })
							 )
					 )
					                :
					 (
						 unknownPos.push({ [ind]: { innerKey: objIn[innerKey] } })
					 )
					);
				});
			});
		}
		else if (childArray.length === 1) {
			if (Object.keys(childArray[0]).length > 1) {
				Object.keys(childArray[0]).map(key => {
					key === "$" ? res.push(childArray[0]["$"]) : null;
				});
			}
			else if (Object.keys(childArray[0]).length === 1 && Object.keys(childArray[0]).indexOf("$") > -1) {
				res.push(childArray[0]["$"]);
			}
			else {

			}
		}
		else {
			return;
		}
		return res;
	}
	else {
		return childArray["$"];
	}
};
let valLevel = 0;
const validateAsync = async (obj, dgb = false) => {
	let result = [];
	valLevel++;
	for( let element in obj ) {
		const value = obj[element];
		let helpObj = {
			elementTag: '',
			attr:       {},
			child:      [],
		};
		let childLength;

		if (!dgb) {
			console.log(chalk.bold.cyan("===>> check element:", JSON.stringify(element), "on Validation-Level =>", `${valLevel} \n`));
			//console.log(chalk.bold.cyan(`=>\t with value ${JSON.stringify(value)}`));
		}

		if (typeof element === 'string' && elementNames.indexOf(element) > -1) {
			helpObj.elementTag = element;

			const valCopy = new Array(value);
			const elementAttributes = getAttributeFromChildObj(valCopy[0], dgb);
			console.log(chalk.yellow("elementAttributes=>", JSON.stringify(elementAttributes)));

			if (elementAttributes instanceof Array && elementAttributes.length > 1) {
				let attributesCounter = 0;
				do {
					let children = [];
					if (elementAttributes[attributesCounter].children !== undefined && elementAttributes[attributesCounter].children.length > 0) {
						childLength = elementAttributes[attributesCounter].children.length;
						console.log(chalk.red(`found childLength = ${childLength}`));

						let innerChildCounter = 0;
						do {
							const validatedChild = await validateAsync(elementAttributes[attributesCounter].children[innerChildCounter])
								.catch((e) => console.error("validateAsync_childrenOf_elemAttr", e));
							children = children.concat(validatedChild);
							innerChildCounter++;
						}
						while( innerChildCounter < elementAttributes[attributesCounter].children.length );
					}
					else {
						childLength = children.length;
					}

					delete elementAttributes[attributesCounter].children;
					//console.log(chalk.greenBright(`mapped_elementAttributes => ${JSON.stringify(elementAttributes[attributesCounter])}`));
					const elemHelpObj = {
						elementTag: element,
						attr:       elementAttributes[attributesCounter],
						child:      children,
					};

					if (elemHelpObj.child !== undefined && childLength === elemHelpObj.child.length) {
						if (attributesCounter === elementAttributes.length - 2) {
							helpObj = elemHelpObj;
						}
						else {
							result.push(elemHelpObj);
						}
						attributesCounter++;
					}
				}
				while( attributesCounter < (elementAttributes.length - 1) );
			}
			else {
				if (elementAttributes instanceof Array) {
					helpObj.attr = elementAttributes[0];
				}
				else {
					helpObj.attr = elementAttributes;
				}
			}
		}
		else if (typeof element === 'string' && element === "_") {
			return value;
		}

		if (value instanceof Array) {
			let childCounter = 0;
			do {
				//console.log(`value of array[${childCounter}] => ${JSON.stringify(value[childCounter])}`);
				childCounter++;
			}
			while( childCounter < value.length );
		}
		else
		if (value instanceof Object && element !== "$" && elementNames.indexOf(element) > -1) {
			if (dgb) {
				console.log(chalk.greenBright("value of", element, "is OBJECT"));
			}
			const childObj = Object.assign({}, value);
			delete childObj["$"];
			console.log("will validate childObj =>", childObj);
			helpObj.child = await validateAsync(childObj, dgb).catch((e) => console.error("validateAsync_childObj ERROR=>", e));
		}

		//console.log(chalk.magenta("helpObj=>", JSON.stringify(helpObj)));
		if (helpObj.elementTag !== '' &&
		    (helpObj.child.length === childLength || Object.keys(helpObj.attr).length > 0)
		) {
			result.push(helpObj);
		}
	}

	//console.log("obj =>", JSON.stringify(obj));
	//console.log("resultLength =", result.length, "result ?>", JSON.stringify(result));
	if (result.length > 0 && result[result.length - 1].elementTag !== ''
	    && (result[result.length - 1].child.length > 0 || Object.keys(result[result.length - 1].attr).length > 0)
	) {
		if (dgb) {
			console.log(chalk.green("stringify return element-->", JSON.stringify(result)));
		}
		valLevel--;
		return (result);
	}
	else {
		valLevel--;
		//console.log(chalk.yellowBright.bgBlack(`result don't compare conditions=> ${JSON.stringify(result)} \n`));
		return [];
	}
};
const validate = (obj, dgb = false) => {
	let result = [];
	for( let element in obj ) {
		const value = obj[element];
		let helpObj = {
			elementTag: '',
			attr:       {},
			child:      [],
		};
		let childLength;

		if (dgb) {
			console.log(chalk.bold.cyan("===>> check element:", JSON.stringify(element), "for arrayPos =>", `${result.length + 1}`));
		}

		if (typeof element === 'string' && elementNames.indexOf(element) > -1) {
			helpObj.elementTag = element;

			const valCopy = new Array(value);
			const elementAttributes = getAttributeFromChildObj(valCopy[0], dgb);
			//console.log(chalk.magenta("elementAttributes=>", JSON.stringify(elementAttributes)));
			if (elementAttributes instanceof Array && elementAttributes.length > 1) {
				const promisedElemHelpObj = [];
				for( let obj of elementAttributes ) {
					console.log(chalk.red("children To Validate =>", JSON.stringify(obj.children)));
					const children = validate(obj.children);
					childLength = children.length;
					delete obj.children;
					const elemHelpObj = {
						elementTag: element,
						attr:       obj,
						child:      children,
					};
					//console.log(chalk.blue.dim("elemAttr_Map=>", JSON.stringify(elementAttributes[i]), `objWillPush_elHelpObj=> ${JSON.stringify(elemHelpObj)}`));
					if (elemHelpObj.child !== undefined) {
						promisedElemHelpObj.push(elemHelpObj);
					}
					else {
						setTimeout(() => {
							if (elemHelpObj.child !== undefined) {
								console.log("push elemHelpObj after Timeout..");
								promisedElemHelpObj.push(elemHelpObj);
							}
							else {
								console.log("timeout are to short!!");
							}
						}, 200);
					}
				}

				promisedElemHelpObj.map((innerObj, index) => {
					console.log(chalk.yellowBright("promisedElemHelpObj => innerObj=", JSON.stringify(innerObj)));
					if (index < promisedElemHelpObj.length) {
						result.push(innerObj);
					}
					else {
						helpObj = innerObj;
					}
				});

				/*const lastAttrChildren = elementAttributes[elementAttributes.length - 1].children;
				 delete elementAttributes[elementAttributes.length -1].children;
				 helpObj.attr = elementAttributes[elementAttributes.length -1];
				 helpObj.child = await validate(lastAttrChildren);*/
			}
			else {
				if (elementAttributes instanceof Array) {
					console.log(chalk.yellow("elemAttr are a array =>", JSON.stringify(elementAttributes[0])));
					helpObj.attr = elementAttributes[0];
				}
				else {
					helpObj.attr = elementAttributes;
				}
			}
		}
		else if (typeof element === 'string' && element === "_") {
			return value;
		}

		if (value instanceof Object && element !== "$" && elementNames.indexOf(element) > -1) {
			if (dgb) {
				console.log(chalk.greenBright("value of", element, "is OBJECT"));
			}
			const childObj = Object.assign({}, value);
			delete childObj["$"];
			helpObj.child = validate(childObj, dgb);
		}

		//console.log(chalk.magenta("helpObj=>", JSON.stringify(helpObj)));
		if (helpObj.elementTag !== '' &&
		    (helpObj.child.length === childLength || Object.keys(helpObj.attr).length > 0)
		) {
			result.push(helpObj);
		}
	}

	//console.log("obj =>", JSON.stringify(obj));
	//console.log("resultLength =", result.length, "result ?>", JSON.stringify(result));
	if (result.length > 0 && result[result.length - 1].elementTag !== ''
	    && (result[result.length - 1].child.length > 0 || Object.keys(result[result.length - 1].attr).length > 0)
	) {
		if (dgb) {
			console.log(chalk.green("stringify return element-->", JSON.stringify(result)));
		}
		return result;
	}
	else {
		//console.log(chalk.yellowBright.bgBlack(`result don't compare conditions=> ${JSON.stringify(result)} \n`));
		return [];
	}
};

const validateParsedObject = async (parsedObject, debugOption = false) => {
	let modifiedObject = {
		validatedImports: [],
		validatedObjects: [],
	};

	/* validate elements for later import */
	modifiedObject.validatedImports = filterDuplicates(validateElementsToImport(parsedObject['Svg'], debugOption), debugOption);
	/* validate und refactor svg object */
	modifiedObject.validatedObjects = await validateAsync(parsedObject, debugOption).then(validObjects =>
		modifiedObject.validatedObjects = validObjects,
	);
	//modifiedObject.validatedObjects = parsedObject;//validate(parsedObject);

	return modifiedObject;
};
export default validateParsedObject;