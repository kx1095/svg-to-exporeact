/** cliProgram.js  -  SVG-to-ExpoReact
 * this command line program script was written and developed by KX1095. See license.txt for using rights.
 * (c) 2018 Hans-Peter Kern. all rights reserved.
 */
'use strict';
import chalk                       from 'chalk';
import Enquirer                    from 'enquirer';
import fs                          from 'fs';
import glob                        from 'glob';
import osLocale                    from 'os-locale';
import path                        from 'path';
import { parseExample, parseFile } from "./parseSvgXml";

const enqui = new Enquirer();

/*if (args.help) {
 console.log(content.helpText);
 process.exit(1);
 }
 if (args.example) {
 console.log(content.exampleText);
 process.exit(1);
 }*/
const fileSelectPrompt = async (translations) => {
	const filesFromHere = glob.sync("*/*.svg", {});
	const selectedFile = await enqui.prompt({
		type:    'select',
		name:    'file',
		message: translations.prompt_selectFileToConvert,
		choices: filesFromHere,
		initial: filesFromHere[0],
	}).catch(console.error);

	if (selectedFile === undefined) {
		return filesFromHere[0];
	}
	return selectedFile.file;
};
const checkIfFolder = (file) => {
	try {
		const filePath = path.resolve("./" + file);
		return fs.statSync(filePath).isDirectory();
	}
	catch(e) {
		console.error(e);
		return false;
	}
};

const exitConvertCli = (translations, message) => {
	if (message === "fail") {
		const message = translations.exitMessageFail;
		console.log(chalk.redBright(message));
	}
	else {
		const successMessage = translations.exitMessageSuccess;
		console.log(chalk.keyword('green')(successMessage));
	}
	process.exit();
};

let customName, fileToConvert, dirToConvert;

const run = async () => {
	let translations;
	const locale = await osLocale();
	if (locale === "de_DE") {
		translations = require("../lang/de");
	}
	// else -> default english
	else {
		translations = require("../lang/en");
	}

	/* Show welcome message and ask for task. TODO: quick-actions can pass */
	console.log(translations.startText);
	const convertTask = await enqui.prompt({
		type:    "select",
		name:    "task",
		message: translations.prompt_convertTask,
		choices: translations.answers_convertTask,
	}).catch(console.error);

	/* check the answer and show prompt for customizing final component name */
	let customFilenamePrompt;
	switch(convertTask.task) {
		/** parse single file **/
		case 0 || "0":
			if (fileToConvert === undefined) {
				fileToConvert = await fileSelectPrompt(translations);
				if (fileToConvert === undefined) {
					exitConvertCli(translations, 'fail');
				}
			}
			customFilenamePrompt = await enqui.prompt({
				type:    'select',
				name:    'customFilenameBool',
				message: translations.prompt_customFilenameBool,
				choices: translations.answer_boolYesNo,
				initial: false,
			}).catch(console.error);
			break;
		/**TODO: parseFolder **/
		case 1:
			console.log(
				chalk.keyword('yellow')("Noch nicht implementiert, aber ich bin dran.. :->"),
			);
			process.exit();
			break;
		/** parse example **/
		case 2:
			const exampleProps = { isExample: true, debug: false, force: true, outPath: process.cwd() };
			await parseExample('./test/example.svg', "Example", exampleProps, translations);
			break;
		case 3:
			console.log(
				chalk.keyword('red')("\nSehr gute Idee!"),
				" - ",
				chalk.blue("Auf zu André, der Kaffee-trink-Nachbar deiner Region ;-D"),
			);
			process.exit();
			break;
		default:
			console.error(
				chalk.gray.bgRed("Can't validate your answer.\n"),
				translations.exitMessageFail,
			);
			process.exit();
			break;
	}

	/* Ask user for customizing component- and filename */
	if (customFilenamePrompt !== undefined && customFilenamePrompt.customFilenameBool === 'true') {
		const customNamePrompt = await enqui.prompt({
			type:     'input',
			name:     'customCompName',
			message:  translations.prompt_editFilename,
			initial:  path.basename(fileToConvert, '.svg'),
			editable: true,
		}).catch(console.error);
		customName = customNamePrompt.customCompName;
	}else
	if (fileToConvert !== undefined) {
		customName = path.basename(fileToConvert, '.svg');
	}

	/* Ask for show my debug-logs - just for fun */
	let showDebugPrompt = await enqui.prompt({
		type:    'select',
		name:    'showDgb',
		message: translations.prompt_showDevsDgbLogs,
		choices: translations.answer_boolYesNo,
	}).catch(console.error);

	parseFile(fileToConvert,
		customName,
		{ outPath: '', force: false, debug: showDebugPrompt.showDgb !== 'false', isExample: false },
		translations,
	);
};

run().catch(console.error);