/** generateExpoReact.js  -  SVG-to-ExpoReact
 * this generator-script was developed by KX1095. please see license.txt for using rights.
 * (c) 2018 Hans-Peter Kern. All rights reserved.
 **/
'use strict';
import chalk       from 'chalk';
import writeToFile from "./writeToFile";

const defaultSvgTouchEvents = ["disabled", "onPress", "onPressIn", "onPressOut", "onLongPress", "delayPressIn", "delayPressOut", "delayLongPress"];
const modifiableAttributes = ["id", "width", "height", "r", "fill", "stroke", "x", "y", "cx", "cy", "r", "rx", "ry", "fx", "fy", "stopColor", "offset", "opacity"];
const validAttributes = [
	"fill", "fillOpacity", "fillRule", "stroke", "strokeWidth", "strokeOpacity", "strokeLinecap", "strokeLinejoin", "strokeDasharray", "strokeDashoffset",
	"rotation", "x", "y", "originX", "originY", "origin", "scale", "viewBox", "id", "cx", "cy", "r", "rx", "ry", "fx", "fy", "width", "height", "textAnchor",
	"fontFamily", "fontSize", "fontWeight", "dy", "dx", "href", "points", "x1", "x2", "y1", "y2", "d", "preserveAspectRatio", "clipPath", "offset", "opacity",
	"stopColor", "stopOpacity", "gradientUnits", "maskUnits", "patternUnits", "mask", "transform", "style"
];
let modifiableAttributeProps = [];

const pushDefaultSvgTouchEvents = (elementID) => {
	defaultSvgTouchEvents.map(attribute => {
		let identifier = elementID + "_" + attribute;
		modifiableAttributeProps.push({
			[identifier]: attribute === "" ? false : null,
		});
	});
};
const splitStyleString = (styleString) => {
	const keyValArr = styleString.split(";");
	let lv2SplitArr = keyValArr.map(keyVal => {
		return keyVal.split(":");
	});
	let styleStrObj = {};
	lv2SplitArr.forEach(item => styleStrObj[item[0]] = item[1]);
	if (Object.keys(styleStrObj).length > 0) {
		return styleStrObj;
	}
};
const checkValueIsValidNumber = (value) => {
	if (!isNaN(value)) {
		return +value;
	}
	else {
		const valueEnd = value.substring(value.length-2, value.length);
		if (valueEnd.indexOf("px") > -1) {
			return +(value.substring(0, value.length - 2));
		}
		else {
			return value;
		}
	}
};
const getStringsOfAttrObject = (attrObj, dbg, elemId, tabs) => {
	let retStr = "";
	let arrForPrototypes = [];
	if (attrObj instanceof Object) {
		Object.keys(attrObj).map(attrKey => {
			if (validAttributes.indexOf(attrKey) > -1) {
				const tryValueToNumber = checkValueIsValidNumber(attrObj[attrKey]);
				if (dbg) {
					console.log(chalk.yellow("tryValueToNumber =>", tryValueToNumber, `isNaN= ${isNaN(tryValueToNumber)}`));
				}
				if (elemId !== undefined) {
					retStr += `${tabs !== undefined ? tabs : ''}${attrKey}={props.${elemId}_${attrKey} !== undefined ? props.${elemId}_${attrKey} : ${ isNaN(tryValueToNumber) ? `"${attrObj[attrKey]}"` : tryValueToNumber }}\n`;
					arrForPrototypes.push({[elemId +"_"+ attrKey]: (isNaN(tryValueToNumber) ? `"${attrObj[attrKey]}"` : tryValueToNumber) })
				}
				else {
					retStr += `${tabs !== undefined ? tabs : ''}${attrKey}={${ isNaN(tryValueToNumber) ? `'${attrObj[attrKey]}'` : tryValueToNumber }}\n`;
					//arrForPrototypes.push({ [elemId + "_" + attrKey]: (isNaN(tryValueToNumber) ? `'${attrObj[attrKey]}'` : tryValueToNumber) })
				}
			}
		}).join('');
	}
	return {
		attrStr: retStr,
		protoArr: arrForPrototypes
	};
};

let tabCounter = 0;
const extractSvgObject = (object, dbg) => {
	if (Array.isArray(object)) {
		const extractedItem = object.map(item => {
			return extractSvgObject(item, dbg);
		}).join('');
		if (extractedItem !== undefined) {
			return extractedItem;
		}
	} else
	if (object instanceof Object) {
		tabCounter++;
		const expoTagName = object.expoTag !== undefined ? object.expoTag : 'NOT:SUPPORTED';
		if (dbg) {
			console.log(chalk.magenta("Extract Element:", expoTagName, `tabCounter = ${tabCounter} obj=>${JSON.stringify(object)}`));
		}
		let tabsStr = ``;
		for (let t = 0; t < tabCounter-1; t++) {
			tabsStr += `\t`;
		}

		let attrs;
		if (object.attributes !== undefined && Object.keys(object.attributes).length > 0) {
			attrs = Object.keys(object.attributes).map((attr, index, attrsList) => {
				let attrStr = ``;
				let tabs = '';
				for( let tN = 0; tN < tabCounter + 2; tN++ ) {
					tabs += `\t`;
				}
				if (attr === "style") {
					const parsedStyles = object.attributes[attr];
					//console.log("parsedStyles_obj.attributes{attr} =>", parsedStyles);
					const splitted = splitStyleString(parsedStyles);
					let validatedAttributesFromStyles;
					if (attrsList.indexOf("id") > -1) {
						const resObj = getStringsOfAttrObject(splitted, dbg, object.attributes['id'], tabs);
						validatedAttributesFromStyles = resObj.attrStr;
						modifiableAttributeProps.push(...resObj.protoArr);
					}
					else {
						const resObject = getStringsOfAttrObject(splitted, dbg, undefined, tabs);
						validatedAttributesFromStyles = resObject.attrStr;
					}
					attrStr += '\n'+ tabs + validatedAttributesFromStyles + `\n${tabs}style={""}`;

				}else
				if (modifiableAttributes.indexOf(attr) > -1 && attrsList.indexOf("id") > -1) {
					const attrIdentifier = object.attributes['id'] + "_" + attr;
					if (attr !== "id") {
						if (dbg) {
							console.log(chalk.yellow(`Add Attribute "${attr}" to Element "${expoTagName}" of component.`));
						}
						attrStr += `\n${tabs}${attr}={${`props.${attrIdentifier} !== undefined ? props.${attrIdentifier} : "${object.attributes[attr]}"`}}`;
						modifiableAttributeProps.push({ [attrIdentifier]: object.attributes[attr] });
					}else
					if (attr === "id") {
						if (dbg) {
							console.log(chalk.yellowBright(`Add Attribute "${attr}" to Element "${expoTagName}" of component.`));
						}
						attrStr += `\n${tabs}${attr}={"${object.attributes[attr]}"}`;
					}
					if (object.expoTag === "Mask" ) {
						if (attrsList.indexOf("width") < 0) {
							const maskWidthIndent = object.attributes['id'] + "_width";
							attrStr += `\nwidth={props.${maskWidthIndent} !== undefined ? props.${maskWidthIndent} : '100%'}`;
							modifiableAttributeProps.push({ [maskWidthIndent]: '100%' });
						}
						if (attrsList.indexOf("height") < 0) {
							const maskHeightIndent = object.attributes['id'] + "_height";
							attrStr += `\nheight={props.${maskHeightIndent} !== undefined ? props.${maskHeightIndent} : '100%'}`;
							modifiableAttributeProps.push({ [maskHeightIndent]: '100%' });
						}
						if (attrsList.indexOf("x") < 0) {
							const maskXIndent = object.attributes['id'] + "_x";
							attrStr += `\nx={props.${maskXIndent} !== undefined ? props.${maskXIndent} : 0}`;
							modifiableAttributeProps.push({ [maskXIndent]: '0'});
						}
						if (attrsList.indexOf("y") < 0) {
							const maskYIndent = object.attributes['id'] + "_y";
							attrStr += `\ny={props.${maskYIndent} !== undefined ? props.${maskYIndent} : 0}`;
							modifiableAttributeProps.push({ [maskYIndent]: '0'});
						}
					}

					/*if (index - 1 === Object.keys(object.attributes).length) {
						attrStr += defaultSvgTouchEvents.map(attribute => `${attribute}={props.${object.attributes['id']}_${attribute} !== undefined ? props.${object.attributes['id']}_${attribute} : ${attribute === 'disabled' ? `false`: `null`}`).join('\n\t\t');
						pushDefaultSvgTouchEvents(object.attributes['id']);
					}*/
				}
				else {
					if (dbg) {
						console.log(chalk.yellowBright(`There is no ID in Element ->${object.expoTag}, set static attributes of ${attr}`));
					}
					if (attr.length > 0 && (attr !== undefined && attr !== 'undefined')) {
						attrStr += `\n${tabs}${attr}={"${object.attributes[attr]}"}`;
					}
				}
				return attrStr;
			}).join('');
		}

		let childItems, textItem;
		if (object.children !== undefined && object.children.length > 0) {
			childItems = object.children.map((child, index) => {
					tabCounter = tabCounter-index;
					 return extractSvgObject(child, dbg);
				})
                   .filter(child => child !== undefined)
                   .join('\n');
			if (object.children[0].text !== undefined && object.children[0].text.length > 0) {
				textItem = object.children[0].text;
			}
		}

		let returnStr = "";
		if (object.expoTag !== undefined && object.expoTag.length >0) {
			returnStr += tabsStr +"<"+ object.expoTag;
			tabsStr += "\t\t";

			if (attrs !== undefined && attrs.length > 0) {
				returnStr += attrs +"\n";
			}
			// Check existence of childItems and add
			if (childItems !== undefined && childItems.length > 0 && object.expoTag !== "Text") {
				returnStr += tabsStr + ">\n" + tabsStr + "\t" + childItems + "\n" + tabsStr + "</" + object.expoTag + ">";
			}else
			// Check existence of textItem and add
			if (textItem !== undefined && textItem.length > 0 ) {
				returnStr += tabsStr +">" + tabsStr + textItem + tabsStr + "</" + object.expoTag + ">";
			}
			else {
				returnStr += "/>";
			}
			tabCounter--;
		}
		return returnStr;
	}
	else {
		return false;
	}
};
const generatePropTypes = () => {
	return modifiableAttributeProps.map(attributesOfElement => {
		return Object.keys(attributesOfElement).map(attrKey => {
			return `${attrKey}: PropTypes.${typeof attributesOfElement[attrKey]}`;
		});
	}).join(',\n\t');
};
const getDefaultProps = () => {
	return modifiableAttributeProps.map(attributesOfElement => {
		return Object.keys(attributesOfElement).map(attrKey => {
			const attributeValue = attributesOfElement[attrKey];
			const tryToNum = checkValueIsValidNumber(attributeValue);
			if (isNaN(tryToNum)) {
				if (tryToNum.indexOf("\"") > -1) {
					return `${attrKey}: ${tryToNum}`;
				}
				return `${attrKey}: "${tryToNum}"`;
			}
			return `${attrKey}: ${tryToNum}`;
		});
	}).join(',\n\t');
};

export const debugExpoReact = async (svgObject, componentName, options, translations) => {
	const generatedCode = await generateExpoReact(svgObject, componentName, options);
	//console.log("generatedHTML ==>\n", generatedCode);
	writeToFile(generatedCode, componentName, options, translations);
};

const generateExpoReact = async (svgObject, componentName, options, translations) => {
	const renderContent = extractSvgObject(svgObject.children, options.debug);
	const fileContent = `/** this component was created with svg-to-exporeact by KX1095. **/
import React        from 'react';
import PropTypes    from 'prop-types';
import {Svg}        from 'expo';
		
const {${svgObject.validatedImports.map(item => {
		if (item !== "Svg") {
			return item + ",";
		}
	}).join(" ")}} = Svg;
	
const ${componentName} = (props) => {
	return (			
		${renderContent}
	);
};

${componentName}.propTypes = {
	${
		await generatePropTypes()
		}
};

${componentName}.defaultProps = {
	${
		await getDefaultProps()
		}
};
export default ${componentName};
	`;

	if (options.debug || options.isExample) {
		return fileContent;
	}
	else {
		writeToFile(fileContent, componentName, options, translations);
	}
};
export default generateExpoReact;
