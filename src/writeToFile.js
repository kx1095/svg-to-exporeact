/** writeToFile.js  -  SVG-to-ExpoReact
 * this script was developed by KX1095 for 'svg-to-exporeact'-npmPackage. please see license.txt for using rights.
 * (c) 2018 Hans-Peter Kern. All rights reserved.
 **/
'use strict';
import chalk from 'chalk';
import fs    from 'fs';
import path  from 'path';

module.exports = (expoComponent, componentFileName, writeOptions, translations) => {
	let filePath;
	if (writeOptions.hasOwnProperty("outPath") && writeOptions.outPath !== '') {
		filePath = path.resolve(process.cwd(), writeOptions.outPath, componentFileName + ".js");
	}
	else {
		filePath = path.resolve(process.cwd(), componentFileName + ".js");
	}

	console.log(chalk.greenBright(translations.writeToFile_Success, filePath));

	fs.writeFile(filePath, expoComponent, { flag: (writeOptions.hasOwnProperty('force') && writeOptions.force) ? 'w' : 'wx' }, (error) => {
		if (error) {
			if (writeOptions.hasOwnProperty('debug') && writeOptions.debug) {
				console.log(chalk.yellow.bgRed(`Can't write: ERR: ${error}`));
			}
			return; //false;
		}
		return '';
	});
};