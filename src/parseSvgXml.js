/** parseSvgXml.js  -  SVG-to-ExpoReact
 * this parsing-script was developed by KX1095. please see license.txt for using rights.
 * (c) 2018 Hans-Peter Kern. All rights reserved.
 **/
'use strict';
import chalk                                 from 'chalk';
import fs                                    from 'fs';
import path                                  from 'path';
import generateExpoReact, { debugExpoReact } from "./generateExpoReact";
import writeToFile                           from "./writeToFile";

const convert                                   = require('xml-js');

const supportedProps = [
	"fill", "fillOpacity", "fillRule", "stroke", "strokeWidth", "strokeOpacity", "strokeLinecap", "strokeLinejoin", "strokeDasharray", "strokeDashoffset",
	"rotation", "x", "y", "originX", "originY", "origin", "scale", "viewBox", "id", "cx", "cy", "r", "rx", "ry", "fx", "fy", "width", "height", "textAnchor",
	"fontFamily", "fontSize", "fontWeight", "dy", "dx", "href", "points", "x1", "x2", "y1", "y2", "d", "preserveAspectRatio", "clipPath", "offset", "opacity",
	"stopColor", "stopOpacity", "gradientUnits", "maskUnits", "patternUnits", "mask", "transform", "style"
];
const elementNames = [
	"Circle",
	"Ellipse",
	"G",
	"Text",
	"TSpan",
	"TextPath",
	"Path",
	"Polygon",
	"Polyline",
	"Line",
	"Rect",
	"Use",
	"Image",
	"Symbol",
	"Defs",
	"LinearGradient",
	"RadialGradient",
	"Stop",
	"ClipPath",
	"Pattern",
	"Mask",
	"Svg",
];
const capitaliseTagNames = (name) => {
	let capitalisedName;
	const lowercaseRest = name.slice(1,name.length);
	capitalisedName = name.charAt(0).toUpperCase() + lowercaseRest;
	return capitalisedName;
};
const moddedValidAttributes = [];
const validateAttr = (attr, dbg) => {
	if (supportedProps.indexOf(attr) > -1 && attr.indexOf(":") < 0) {
		if (dbg) {
			console.log(chalk.blueBright("attr =>", attr, "is valid.."));
		}
		return attr;
	}else
	if (attr.indexOf("-") > -1) {
		const indexOfMinus = attr.indexOf("-");
		const subStrBefore = attr.substring(0, indexOfMinus);
		const subStrAfter = attr.substring(indexOfMinus + 1, attr.length);
		const capitalizedSubAfter = subStrAfter[0].toUpperCase() + subStrAfter.substring(1, subStrAfter.length);
		const modAttrStr = subStrBefore + capitalizedSubAfter;
		if (supportedProps.indexOf(modAttrStr) > -1 && modAttrStr.indexOf(":") < 0) {
			if (dbg) {
				console.log("modified attribute:", modAttrStr, "is also valid attr..");
			}
			moddedValidAttributes.push(modAttrStr);
			return modAttrStr;
		}
		if (dbg)  {
			console.log(chalk.red("attribute =>", attr, "don't supported.."));
		}
		return undefined;
	}else
	if (attr.indexOf(":") < -1) {
		if (dbg) {
			console.log(chalk.red("attribute =>", attr, "don't supported.."));
		}
		return undefined;
	}
	if (dbg) {
		console.log(chalk.red("attribute =>", attr, "don't supported.."));
	}
	return undefined;
};
Object.defineProperty(Array.prototype, 'flat', {
	value: function(depth = 1) {
		return this.reduce(function(flat, toFlatten) {
			return flat.concat((Array.isArray(toFlatten) && (depth - 1)) ? toFlatten.flat(depth - 1) : toFlatten);
		}, []);
	},
});
const filterDuplicates = (arr, dbg) => {
	let unique = {};
	console.log();
	const flatted = arr.flat(10);
	if (dbg) {
		console.log(chalk.gray(`check validated imports for duplicates in => ${flatted}`));
	}
	return flatted.filter((item) => {
		return unique.hasOwnProperty(item) ? false : (unique[item] = true);
	});
};
// convert xml file content to js-object and validate.
const convertSvgFile = async (data, options) => {
	const validObj4Import = [];
	const jsObj = await convert.xml2js(data, {
		compact:              false,
		ignoreDeclaration:    true,
		ignoreCdata:          true,
		ignoreDoctype:        true,
		nativeTypeAttributes: false,
		nameKey:              'expoTag',
		elementsKey:          'children',
		elementNameFn:        elemTag => {
			if (elementNames.indexOf(elemTag) > -1 || elementNames.indexOf(capitaliseTagNames(elemTag)) > -1) {
				validObj4Import.push(capitaliseTagNames(elemTag));
				if (options.debug) {
					console.log("[elemTag]:", elemTag, "is valid for Expo.. ");
				}
				return capitaliseTagNames(elemTag);
			}
			return '';
		},
		attributeNameFn:      attrTag => {
			const validAttrTag = validateAttr(attrTag, options.debug);
			if (validAttrTag !== undefined && validAttrTag !== "" && validAttrTag.length > 0) {
				if (options.debug) {
					console.log(chalk.greenBright("have validate attrTag =>", validAttrTag));
				}
				return validAttrTag;
			}
			return undefined;
		},
		attributeValueFn:     (attrValue, attrKey) => {
			let validAttrs = {};
			if (options.debug) {
				console.log(chalk.yellow("will validate attrVal with attrKey now =>", attrKey, ":", attrValue));
			}
			const validAttrTag = validateAttr(attrKey, options.debug);
			if (validAttrTag !== undefined && validAttrTag !== "undefined" && validAttrTag !== false && validAttrTag.length > 0) {
				Object.assign(validAttrs, { [validAttrTag]: attrValue });
			}

			if (validAttrs !== undefined && Object.keys(validAttrs).length > 0) {
				let attributes;
				if (moddedValidAttributes.length > 0 && moddedValidAttributes.indexOf(Object.keys(validAttrs)[0]) > -1) {
					attributes = attrValue;
				}
				else {
					attributes = validAttrs[attrKey];
				}

				if (options.debug) {
					console.log(chalk.blueBright("validAttributes =>", attrKey, ":", attributes));
				}
				if (attributes !== undefined) {
					return attributes;
				}
				return '';
			}
			return '';
		}
	});
	if (jsObj !== undefined && validObj4Import !== undefined) {
		return { jsObj, validObj4Import };
	}
};

const checkIfFolder = (filePath, dbg) => {
	try {
		return fs.statSync(filePath).isDirectory();
	}
	catch(e) {
		if (dbg) {
			console.error(e);
		}
		return false;
	}
};
const checkFileExist = (filePath, dbg) => {
	try {
		return fs.statSync(filePath).isFile()
	}
	catch(e) {
		if (dbg) {
			console.error(e);
		}
		return false;
	}
};
const checkOutFilePath = (filePath, dbg) => {
	try {
		return fs.statSync(filePath).isFile();
	}
	catch(e) {
		if (dbg) {
			if (e.code === "ENOENT") {
				console.log(chalk.keyword('orange')(`File on Path ${filePath} don't exist, so i create one.`));
			}
			else {
				console.error(e);
			}
		}
		return false;
	}
};

const parseExample = (file, filename, options, translations) => {
	console.log("Parse Example File:", file," with props =>", options);
	fs.readFile(file, 'utf8', (err, data) => {
		const { jsObj, validObj4Import } = convertSvgFile(data, options);
		if (jsObj !== undefined && validObj4Import !== undefined) {
			const name = filename ? filename : path.basename(file, '.svg');
			const checkPath = path.resolve(process.cwd(), path.basename(file, '.svg'));

			if (!checkOutFilePath(checkPath, options.debug)) {
				options.outPath = path.resolve(process.cwd(), path.dirname(file));
			}
			const parsedSvgAndValidatedImports = {
				validatedImports: filterDuplicates(validObj4Import, options.debug),
				children:         jsObj.children,
			};

			const convertPromise = generateExpoReact(parsedSvgAndValidatedImports, capitaliseTagNames(name), options, translations);
			if (convertPromise !== undefined) {
				writeToFile(convertPromise, "Example", options, translations);
				console.log(chalk.green(translations.exitMessageSuccess));
				process.exit();
			}
		}
	});
};
const parseFile = (file, filename, options, translations) => {
	if (checkIfFolder(file, options.debug)) {
		console.log(chalk.keyword('orange')(`You choose the wrong option. ${path.basename(file)} is a folder.\nUse the convert folder option.`));
		return;
	}
	else if (!checkFileExist(file, options.debug)) {
		// todo: ENOENT: no file or directory console.error().
		console.log(chalk.red(`${path.basename(file)} don't exist.`));
	}
	fs.readFile(file, 'utf8', async (err, data) => {
		const {jsObj, validObj4Import} = await convertSvgFile(data, options);
		if (jsObj !== undefined && validObj4Import !== undefined) {
			const name = filename ? filename : path.basename(file, '.svg');
			const checkPath = path.resolve(process.cwd(), path.basename(file, '.svg'));

			if (!checkOutFilePath(checkPath, options.debug)) {
				options.outPath = path.resolve(process.cwd(), path.dirname(file));
			}

			const parsedSvgAndValidatedImports = {
				validatedImports: filterDuplicates(validObj4Import, options.debug),
				children:         jsObj.children,
			};
			if (options.debug) {
				const debuggedConvertPromise = new Promise(() =>
					debugExpoReact(parsedSvgAndValidatedImports, capitaliseTagNames(name), options, translations)
				);
			}
			else {
				const  convertPromise = new Promise(() =>
					generateExpoReact(parsedSvgAndValidatedImports, capitaliseTagNames(name), options, translations)
				);
			}
		}
	});
};

export { parseFile, parseExample };