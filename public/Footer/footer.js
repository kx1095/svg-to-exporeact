window.Footer = zoid.create({
    tag: 'footer-component',
    url: 'https://kx1095.gitlab.io/svg-to-exporeact/Footer/footer.htm',
    dimensions: {
        width: '100%',
        height: '100%',
    },
    autoResize: {
        width: false,
        height: true,
    },
});