# SVG-to-ExpoReact

SVG-to-ExpoReact is a small CLI-Tool that convert SVG files into valid Expo-JSX.

This tool generate full valid `*.js` file of your vector graphic.
### Here a small Example
If you have a svg-file that looks like following:
```xml
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
        preserveAspectRatio="xMidYMid meet"
        viewBox="0 0 200 200"
        width="200"
        height="200"
>
    <defs>
        <radialGradient id="grad" cx="50%" cy="50%" fx="50%" fy="50%">
            <stop
                    id="gradStopMid"
                    offset="0%"
                    stop-color="#00f"
                    stop-opacity="1"
            />
            <stop
                    id="gradStopOut"
                    offset="100%"
                    stop-color="#004"
                    stop-opacity="1"
            />
        </radialGradient>
        <clipPath id="textBackground1">
            <circle
                    cx="100"
                    cy="100"
                    r="100"
            />
        </clipPath>
        <mask id="maskTest">
            <g>
                <circle
                        r="100"
                        cy="100"
                        cx="100"
                        fill="#FFFFFF"
                />
                <text
                        font-size="45"
                        font-family="sans-serif"
                        text-anchor="middle"
                        x="100"
                        y="115"
                        fill="#000000"
                        id="exampleText1"
                >
                    Example
                </text>
            </g>
        </mask>
    </defs>
    <rect
            id="rectTestID"
            width="200"
            height="200"
            y="0"
            x="0"
            fill="url(#grad)"
            mask="url(#maskTest)"
    />
</svg>
```
And you use `svg-to-exporeact` (or type: `svgToExpo`)

Then you can modify the out going component name and convert the file to:

**GivenComponentName.js**
```jsx
/** this component was created with svg-to-exporeact by KX1095. **/
import React        from 'react';
import PropTypes    from 'prop-types';
import {Svg}        from 'expo';

const { Defs, RadialGradient, Stop, ClipPath, Circle, Mask, G, Text, Rect,} = Svg;

const GivenComponentName = (props) => {
	return (
		<Svg
			preserveAspectRatio={"xMidYMid meet"}
			viewBox={"0 0 200 200"}
			width={"200"}
			height={"200"}
		>
			<Defs			>
				<RadialGradient
					id={"grad"}
					cx={props.grad_cx !== undefined ? props.grad_cx : "50%"}
					cy={props.grad_cy !== undefined ? props.grad_cy : "50%"}
					fx={props.grad_fx !== undefined ? props.grad_fx : "50%"}
					fy={props.grad_fy !== undefined ? props.grad_fy : "50%"}
				>
					<Stop
						id={"gradStopMid"}
						offset={props.gradStopMid_offset !== undefined ? props.gradStopMid_offset : "0%"}
						stopColor={props.gradStopMid_stopColor !== undefined ? props.gradStopMid_stopColor : "#00f"}
						stopOpacity={"1"}
					/>
					<Stop
						id={"gradStopOut"}
						offset={props.gradStopOut_offset !== undefined ? props.gradStopOut_offset : "100%"}
						stopColor={props.gradStopOut_stopColor !== undefined ? props.gradStopOut_stopColor : "#004"}
						stopOpacity={"1"}
					/>
				</RadialGradient>
				<ClipPath
					id={"textBackground1"}
				>
					<Circle
						cx={"100"}
						cy={"100"}
						r={"100"}
					/>
				</ClipPath>
				<Mask
					id={"maskTest"}
					width={props.maskTest_width !== undefined ? props.maskTest_width : '100%'}
					height={props.maskTest_height !== undefined ? props.maskTest_height : '100%'}
					x={props.maskTest_x !== undefined ? props.maskTest_x : 0}
					y={props.maskTest_y !== undefined ? props.maskTest_y : 0}
				>
					<G		>
						<Circle
							r={"100"}
							cy={"100"}
							cx={"100"}
							fill={"#FFFFFF"}
						/>
						<Text
							fontSize={"45"}
							fontFamily={"sans-serif"}
							textAnchor={"middle"}
							x={props.exampleText1_x !== undefined ? props.exampleText1_x : "100"}
							y={props.exampleText1_y !== undefined ? props.exampleText1_y : "115"}
							fill={props.exampleText1_fill !== undefined ? props.exampleText1_fill : "#000000"}
							id={"exampleText1"}
						>
							Example
						</Text>
					</G>
				</Mask>
			</Defs>
			<Rect
				id={"rectTestID"}
				width={props.rectTestID_width !== undefined ? props.rectTestID_width : "200"}
				height={props.rectTestID_height !== undefined ? props.rectTestID_height : "200"}
				y={props.rectTestID_y !== undefined ? props.rectTestID_y : "0"}
				x={props.rectTestID_x !== undefined ? props.rectTestID_x : "0"}
				fill={props.rectTestID_fill !== undefined ? props.rectTestID_fill : "url(#grad)"}
				mask={"url(#maskTest)"}
			/>
		</Svg>
	);
};

GivenComponentName.propTypes = {
	grad_cx: PropTypes.string,
	grad_cy: PropTypes.string,
	grad_fx: PropTypes.string,
	grad_fy: PropTypes.string,
	gradStopMid_offset: PropTypes.string,
	gradStopMid_stopColor: PropTypes.string,
	gradStopOut_offset: PropTypes.string,
	gradStopOut_stopColor: PropTypes.string,
	maskTest_width: PropTypes.string,
	maskTest_height: PropTypes.string,
	maskTest_x: PropTypes.string,
	maskTest_y: PropTypes.string,
	exampleText1_x: PropTypes.string,
	exampleText1_y: PropTypes.string,
	exampleText1_fill: PropTypes.string,
	rectTestID_width: PropTypes.string,
	rectTestID_height: PropTypes.string,
	rectTestID_y: PropTypes.string,
	rectTestID_x: PropTypes.string,
	rectTestID_fill: PropTypes.string
};

GivenComponentName.defaultProps = {
	grad_cx: "50%",
	grad_cy: "50%",
	grad_fx: "50%",
	grad_fy: "50%",
	gradStopMid_offset: "0%",
	gradStopMid_stopColor: "#00f",
	gradStopOut_offset: "100%",
	gradStopOut_stopColor: "#004",
	maskTest_width: "100%",
	maskTest_height: "100%",
	maskTest_x: 0,
	maskTest_y: 0,
	exampleText1_x: 100,
	exampleText1_y: 115,
	exampleText1_fill: "#000000",
	rectTestID_width: 200,
	rectTestID_height: 200,
	rectTestID_y: 0,
	rectTestID_x: 0,
	rectTestID_fill: "url(#grad)"
};
export default GivenComponentName;
```


# Installation of `svg-to-expo`
Make sure you are installed Node.js and `npm` is added to your PATH.

Then open your terminal and type the following:
```
npm i -g svg-to-exporeact
```
OR:
```
npm install -g svg-to-exporeact
```


You can also install it to actual project with:
```
npm i --save svg-to-exporeact
```

