# DEPENCIES DESCRIPTION

 * [Chalk](https://www.npmjs.com/package/chalk) - for colorizing the output
 * [Clui](https://www.npmjs.com/package/clui) - to add some additional visual components (e.g. command-line tables, gauges and spinners)
 * [clear](https://www.npmjs.com/package/clear) -  to clean the console after work
 * [Readline](https://nodejs.org/api/readline.html) - Node-Api to read console-line input
 * [Minimist](https://www.npmjs.com/package/minimist) - for parsing command-line input into object
 * [configstore](https://www.npmjs.com/package/configstore) - for creation and use of simple configurations
 * [Enquire](https://www.npmjs.com/package/enquirer) - really nice cli prompt-tool.
